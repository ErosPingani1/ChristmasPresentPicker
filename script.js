function regaloneNatalone(eljugador) {
    document.getElementById('caricamento').style.display = "block";
    let fortunello = {}
    const url = 'https://christmas-present-picker.herokuapp.com/regalonenatalone?eljugador=' + eljugador;
    const params = {
        headers: {
            "content-type": "application/json; charset=UTF-8"
        },
        mode: 'cors',
        method: 'GET'
    };
    fetch(url, params)
    .then(cors => (cors.json()))
    .then(res => {
        document.getElementById('caricamento').style.display = "block";
        if (res.hasOwnProperty('error')) {
            alert(res.error);
            document.getElementById('eljugador').value = "";
            document.getElementById('caricamento').style.display = "none";
        } else {
            fortunello = res;
            document.getElementById('form').style.display = "none";
            document.getElementById('fortunello').style.display = "block";
            document.getElementById('pic').src = fortunello.pic
            document.getElementById('tip').innerHTML = fortunello.tip
            document.getElementById('caricamento').style.display = "none";
        }
    })
    .catch(err => {
        document.getElementById('caricamento').style.display = "none";
        alert(err);
    });
}